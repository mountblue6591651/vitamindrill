// The function takes an array of items and a vitamin as input,
// returns an array of names for items that contain the specified vitamin.
function getItemByVitamin(items, vitamin) {
  // Use filter to get items that contain the specified vitamin
  let result = items
    .filter((item) => {
      // Check if the item's contains property includes the specified vitamin
      if (item.contains.includes(vitamin)) {
        return true;
      }
    })
    .map((item) => {
      // Map to extract names of items that contain the specified vitamin
      return item.name;
    });

  // Return the array of names for items that contain the specified vitamin
  return result;
}

// Export the getItemByVitamin function to make it available for use in other files
module.exports = getItemByVitamin;
