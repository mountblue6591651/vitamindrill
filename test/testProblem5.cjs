// Import the 'items' array from the '3-arrays-vitamins.cjs' file
const items = require("../3-arrays-vitamins.cjs");

// Import the 'sortItems' function from the 'problem5.cjs' file
const sortItems = require("../problem5.cjs");

// Call the 'sortItems' function with the items array
let result = sortItems(items);

// Log the result (sorted array of items by the number of contained vitamins) to the console
console.log(result);
