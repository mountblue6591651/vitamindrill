// Import the 'items' array from the '3-arrays-vitamins.cjs' file
const items = require("../3-arrays-vitamins.cjs");

// Import the 'getItemByVitamin' function from the 'problem2.cjs' file
const getItemByVitamin = require("../problem2.cjs");

// Call the 'getItemByVitamin' function with the items array
let result = getItemByVitamin(items);

// Log the result (array of names for items that contain "Vitamin C") to the console
console.log(result);
