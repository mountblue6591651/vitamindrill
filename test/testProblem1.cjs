// Import the 'items' array from the '3-arrays-vitamins.cjs' file
const items = require("../3-arrays-vitamins.cjs");

// Import the 'getItems' function from the 'problem1.cjs' file
const getItems = require("../problem1.cjs");

// Call the 'getItems' function with the items array
let result = getItems(items);

// Log the result (array of names for available items) to the console
console.log(result);
