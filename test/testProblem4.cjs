// Import the 'items' array from the '3-arrays-vitamins.cjs' file
const items = require("../3-arrays-vitamins.cjs");

// Import the 'groupItems' function from the 'problem4.cjs' file
const groupItems = require("../problem4.cjs");

// Call the 'groupItems' function with the items array
let groupedItems = groupItems(items);

// Log the result (object grouping item names by their contained vitamins) to the console
console.log(groupedItems);
