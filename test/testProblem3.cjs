// Import the 'items' array from the '3-arrays-vitamins.cjs' file
const items = require("../3-arrays-vitamins.cjs");

// Import the 'getItemByVitamin' function from the 'problem3.cjs' file
const getItemByVitamin = require("../problem3.cjs");

// Specify the vitamin you want to search for
let vitamin = "Vitamin A";

// Call the 'getItemByVitamin' function with the items array and specified vitamin
let result = getItemByVitamin(items, vitamin);

// Log the result (array of names for items that contain the specified vitamin) to the console
console.log(result);
