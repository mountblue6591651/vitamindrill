//The function takes an array of items as input and returns an object grouping item names by their contained vitamins.
function groupItems(items) {
  // Use reduce to group item names by their contained vitamins
  let result = items.reduce(
    (acc, item) => {
      // Split the 'contains' property into an array of vitamins
      let vitamins = item.contains.split(",");

      // Iterate through each vitamin and push the item name to the corresponding group
      vitamins.forEach((element) => {
        // Trim any leading or trailing whitespaces from the vitamin name
        acc[element.trim()].push(item.name);
      });

      // Return the accumulator in each iteration
      return acc;
    },
    {
      "Vitamin A": [],
      "Vitamin B": [],
      "Vitamin C": [],
      "Vitamin D": [],
      "Vitamin K": [],
    }
  );

  // Return the object grouping item names by their contained vitamins
  return result;
}

// Export the groupItems function to make it available for use in other items
module.exports = groupItems;
