//Function takes an array of items as input and returns an array of names for items that contain "Vitamin C".
function getItemByVitamin(items) {
  // Use filter to get items that contain "Vitamin C"
  let result = items
    .filter((item) => {
      // Check if the item contains "Vitamin C"
      if (item.contains === "Vitamin C") {
        return true;
      }
    })
    .map((item) => {
      // Map to extract names of items that contain "Vitamin C"
      return item.name;
    });

  // Return the array of names for items that contain "Vitamin C"
  return result;
}

// Export the getItemByVitamin function to make it available for use in other modules
module.exports = getItemByVitamin;
