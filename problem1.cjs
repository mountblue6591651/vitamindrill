//Function to extract names of available items from an array.
function getItems(items) {
  // Use reduce to accumulate names of available items
  let result = items.reduce((acc, item) => {
    // Check if the item is available and push its name to the accumulator array
    if (item.available === true) {
      acc.push(item.name);
    }
    return acc;
  }, []);

  // Return array of names for available items
  return result;
}

// Export getItems function for use in other files
module.exports = getItems;
