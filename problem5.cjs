//This function takes an array of items as input and returns a new array of items sorted by the number of contained vitamins.
function sortItems(items) {
  // Use the sort method to sort items based on the number of contained vitamins
  let sortedItems = items.sort((item1, item2) => {
    // Calculate the number of contained vitamins for each item
    let vitaminItem1 = item1.contains.split(",").length;
    let vitaminItem2 = item2.contains.split(",").length;

    // Compare the number of contained vitamins for sorting
    if (vitaminItem1 > vitaminItem2) {
      return 1;
    } else if (vitaminItem2 > vitaminItem1) {
      return -1;
    } else {
      return 0;
    }
  });

  // Return the sorted array of items
  return sortedItems;
}

// Export the sortItems function to make it available for use in other files
module.exports = sortItems;
